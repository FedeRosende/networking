#!/usr/bin/python
import sys
from scanner import *

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Send me an IP to execute program. Example: 157.92.32.18")
        sys.exit(0)
    
    ip = sys.argv[1]
    ports = list(range(0, 1026))
    
    scn_config = {'ip': ip, 'ports': ports, 'timeout': 1, 'validate_before': False}
    sn = Scanner(**scn_config)
    sn.run()
