from protocol_communicator import *


class Scanner:
    
    TCP_WELL_KNOWN_PORTS = set([20, 22, 25, 53, 80, 110, 143, 443]) #Puertos que tira https://helpdeskgeek.com/networking/determine-open-and-blocked-ports/
    UDP_WELL_KNOWN_PORTS = set([53, 68, 69, 123, 137, 161, 389, 636]) #Puertos que usa https://www.ipvoid.com/udp-port-scan/
    
    HTTP_HTTPS_PORTS = set([80, 443])
    MAX_TIMEOUT_DIVISION = 100

    def __init__(self, **kwargs):
        self.ip = kwargs['ip']
        self.ports = kwargs['ports']
        self.timeout = kwargs['timeout']
        self.retries = kwargs['retries']
        self.validate_before = kwargs['validate_before']
        self.responses = [dict({'TCP': {}, 'UDP': {}})] * self.retries
        self.verbose = True

    def scan(self, retry_number, protocol):
        print('Scanning {} ports . . .'.format(str(protocol)))
        for dst_port in self.ports:
            src_port = self.src_port()
            resp, status = protocol.communicate(self.ip, src_port, dst_port)
            self.responses[retry_number][str(protocol)][dst_port] = (resp, status)
            if self.verbose:
                print(dst_port, end=" ")
                print(status)

    def src_port(self):
        return random.randint(1025, 65534)  # Cagando al firewall
    
    def run(self):
        if self.validate_before:
            print("Running validation...")
            if self.run_validation():
                print("Validated. Final timeout: {}".format(self.timeout))
            else:
                print("I think you have to change those parameters")
                return
        for i in range(self.retries):
            self.scan(i, protocol=TCPCommunicator(self.timeout))
            self.scan(i, protocol=UDPCommunicator(self.timeout))
            print("Finished retry {}".format(i))
    
    def answer_port(self, ports, protocol):
        for dst_port in ports:
            src_port = self.src_port()
            resp, status = protocol.communicate(self.ip, src_port, dst_port)
            if resp is not None:
                return dst_port
        return None

    def run_validation(self):
        tcp_ports = Scanner.HTTP_HTTPS_PORTS
        udp_ports = None
        
        print("Validating TCP has an HTTP/HTTPS answer port")
        tcp_withresponse = self.answer_port(tcp_ports, protocol=TCPCommunicator(self.timeout))
        udp_withresponse = None #lo dejo comentado por si quiere volver a agregarse
        
        if not (tcp_withresponse or udp_withresponse): 
            return False
        
        i = 1
        j = Scanner.MAX_TIMEOUT_DIVISION
        
        while j - i > 1: #binary search
            divisor = (i + j)//2 #floor division
            new_timeout = round(self.timeout/divisor, 2)
            
            tcp_ok = (not tcp_withresponse) or self.answer_port([tcp_withresponse], protocol=TCPCommunicator(new_timeout))
            udp_ok = (not udp_withresponse) or self.answer_port([udp_withresponse], protocol=UDPCommunicator(new_timeout))
            
            if tcp_ok and udp_ok:
                i = divisor
            else:
                j = divisor
        
        final_divisor = i/2
        self.timeout = min(round(self.timeout/final_divisor, 2), self.timeout)
        return True