import os
import dill

from main import *

class ExpRunner:
    
    @classmethod
    def load_object(cls, file_path, verbose):
        with open(file_path, 'rb') as f:
            if verbose:
                print("File was found. Loading saved data")
            return dill.load(f)

    def __init__(self, sn_config, jobname, save_data=True, prefer_loading_saved_data=True):
        self.sn_config = sn_config
        self.jobname = jobname
        self.save_data = save_data
        self.prefer_loading_saved_data = prefer_loading_saved_data
        self.port_scanner = None

    @property
    def file_dump_path(self):
        
        def want_to_print(k, v):
            return k is not "ports"
        
        filename = 'port_scanner__' + self.jobname
        for k, v in sorted(self.sn_config.items()):
            if want_to_print(k, v):
                filename += '__' + str(k) + '_' + str(v)

        filename += '.pkl'

        return 'data/' + self.jobname + '/' + filename

    def run_exp(self, verbose=True):
        if self.prefer_loading_saved_data and os.path.exists(self.file_dump_path):
            # run exp only if it has not been run before
            self.port_scanner = ExpRunner.load_object(self.file_dump_path, verbose)

        else:
            self.construct_dir()
            self.port_scanner = Scanner(**self.sn_config)
            self.port_scanner.verbose = verbose
            self.port_scanner.run()

        if self.save_data:
            with open(self.file_dump_path, 'wb') as f:
                dill.dump(self.port_scanner, f)

        return self.port_scanner

    def construct_dir(self):
        if self.save_data:
            d = 'data/' + self.jobname
            if not os.path.isdir(d):
                os.mkdir(d)
