import pandas as pd
from exp_runner import ExpRunner
import dill

def load_port_scanners_from_jobname(jobname):
    port_scanners = []
    for port_scanner_filename in os.listdir('data/' + jobname):
        port_scanner_filepath = 'data/' + jobname + '/' + port_scanner_filename
        port_scanners.append(ExpRunner.load_object(port_scanner_filepath, verbose=False))

    return port_scanners


def load_all_port_scanners():
    exp_objects = {}
    for jobname in os.listdir('data'):
        if jobname != 'bokita':
            exp_objects[jobname] = load_port_scanners_from_jobname(jobname)
    return exp_objects

def superdataframemonolitico():
    exps = load_all_port_scanners()
    communications = bokita(exps)

def bokita(exps):
    communications = []
    for jobname, port_scanner_list in exps.items():
        for ps in port_scanner_list:
            for retry_number, retry in enumerate(ps.responses):
                for protocol, scans in retry.items():
                    for port, scan_data in scans.items():
                        _, status = scan_data

                        ps_data = {'jobname': jobname}

                        ps_data['ip'] = ps.ip
                        ps_data['timeout'] = ps.timeout

                        ps_data['retry'] = retry_number
                        ps_data['port'] = port
                        ps_data['protocol'] = protocol
                        ps_data['status'] = status

                        communications.append(ps_data)

    return pd.DataFrame(communications)


# Carga los resultados de la carpeta pedida
def load_data(jobname):
    resps = []
    path = 'data/' + jobname + "/"
    if (os.path.exists(path)):
        filenames = os.listdir(path)
        for name in filenames:
            with open(path+name, 'rb') as f:
                resps.append(dill.load(f))
    return resps


# Devuelve los puertos para cada ip que hayan devuelto el status solicitado para el protcolo pedido
def get_protocol_ports_by_status(resps, protocol, st):
    ports = {}
    for resp in resps:
        ports[resp.ip] = []
        for port, status in resp.responses[0][protocol].items():
            if status[1] == st:
                ports[resp.ip].append(port)
    return ports


# Devuelve la lista de todos los puertos clasificada por el tipo de respuesta obtenido para el protocolo pedido
def get_ports_by_response(resps, protocol, avoid):
    ports = {'open': [], 'filtered': [], 'closed': [], 'unknown': []}
    target = ''
    for resp in resps:
        if(not resp.ip in avoid):
            for port, status in resp.responses[0][protocol].items():
                if (status[1] == 'open' or status[1] == 'open SA'):
                    target = 'open'
                elif (status[1] == 'closed' or status[1] == 'closed AR'):
                    target = 'closed'
                elif (status[1] == 'filtered'):
                    target = 'filtered'
                else:
                    target = 'unknown'
                ports[target].append(port)
    return ports


# Cuenta las apariciones de cada puerto 
def count_ports_appearances(ports_d):
    appearances = {}
    for status, ports in ports_d.items():
        appearances[status] = {}
        for port in ports: 
            appearances[status][port] = 0
        for port in ports:
            appearances[status][port] = appearances[status][port] + 1

    return appearances


def dict_to_arr2(dic):
    arr1 = []
    arr2 = []
    for key, val in dic.items():
        arr1.append(key)
        arr2.append(val)
    return arr1, arr2
