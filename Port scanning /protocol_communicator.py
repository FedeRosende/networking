from scapy.all import *


class ProtocolCommunicator:

    def __init__(self):
        raise Exception("abstract class, cant instantiate")

    def __str__(self):
        return self.name

    def communicate(self):
        raise Exception("abstract class, cant communicate")


class TCPCommunicator(ProtocolCommunicator):

    def __init__(self, timeout):
        self.name = "TCP"
        self.timeout = timeout

    def communicate(self, ip, src_port, dst_port):
        p_tcp = IP(dst=ip)/TCP(sport=src_port, dport=dst_port, flags='S')
        resp = sr1(p_tcp, verbose=False, timeout=self.timeout)

        status = None
        if resp is None:
            status = "filtered" # filtered
        elif resp.haslayer(TCP):
            tcp_layer = resp.getlayer(TCP)
            if tcp_layer.flags == 0x12:
                status = "open {}".format(tcp_layer.flags)
                sr1(IP(dst=ip)/TCP(sport=src_port,dport=dst_port, flags='AR'), verbose=False, timeout=self.timeout)
            elif tcp_layer.flags == 0x14:
                status = "closed {}".format(tcp_layer.flags)
        elif resp.haslayer(ICMP):
            status = "closed" #closed - unreachable

        return resp, status


class UDPCommunicator(ProtocolCommunicator):

    def __init__(self, timeout):
        self.name = "UDP"
        self.timeout = timeout

    def communicate(self, ip, src_port, dst_port):
        p_udp = IP(dst=ip)/UDP(sport=src_port, dport=dst_port)
        resp = sr1(p_udp, verbose=False, timeout=self.timeout)
        status = None
        if resp is None:
            status = "open or filtered"
        elif resp.haslayer(ICMP):
            icmp_layer = resp.getlayer(ICMP)
            error_type = int(icmp_layer.type)
            code = int(icmp_layer.code)
            if error_type == 3:
                if code == 3:
                    status = "closed"
                if code in [1, 2, 9, 10, 13]:
                    status = "filtered"

        elif resp.haslayer(UDP):
            status = "open"
        return resp, status
