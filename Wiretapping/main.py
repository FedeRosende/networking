#!/usr/bin/python

import sys
from scapy.all import *
import math
import csv
import time

S1 = {}
iteraciones = int(sys.argv[1]) if len(sys.argv) > 1 else 10000
save_file = sys.argv[2] if len(sys.argv) > 2 else "result_data.csv"
start_time = time.perf_counter()
color_start = '\033[95m'
color_end = '\033[0m'

print(f"\n{color_start}Corriendo {iteraciones} iteraciones, guardando en {save_file}{color_end}\n")

def guardar_data(S):
    N = sum(S.values())
    simbolos = sorted(S.items(), key=lambda x: -x[1])
    entropia = 0
    for _, k in simbolos:
        entropia -= ((k/N) * math.log2(k/N))
    print("Entropia de la fuente: %.5f" % entropia)
    with open(save_file, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Evento", "Probabilidad", "Informacion"])
        for d,k in simbolos:
            writer.writerow([d, k/N, -math.log2(k/N)])

def mostrar_fuente(S):
    global iteraciones
    if not iteraciones:
        guardar_data(S)
        print("Time: {}".format(time.perf_counter() - start_time))
        sys.exit()
    N = sum(S.values())
    simbolos = sorted(S.items(), key=lambda x: -x[1])
    print("\n".join([ "%s : %.5f" % (d,k/N) for d,k in simbolos ]))
    print("\n")
    iteraciones -= 1


def callback(pkt):
    if pkt.haslayer(Ether):
        dire = "BROADCAST" if pkt[Ether].dst=="ff:ff:ff:ff:ff:ff" else "UNICAST"
        proto = pkt[Ether].type # El campo type del frame tiene el protocolo
        s_i = (dire, proto) # Aca se define el simbolo de la fuente
        if s_i not in S1:
            S1[s_i] = 0.0
        S1[s_i] += 1.0
    mostrar_fuente(S1)

sniff(prn=callback)
