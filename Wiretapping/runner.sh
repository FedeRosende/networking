#!/bin/bash
ITERATIONS=100000
i=0
while true
do
  sudo python3 main.py $ITERATIONS res_$i.csv
  ((i=i+1))
done
