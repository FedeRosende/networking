import os
import requests

import pandas as pd
import pycountry_convert as pc

from statistics import mean

from exp_runner import ExpRunner
import ipaddress
import platform    # For getting the operating system name
import subprocess  # For executing a shell command

class SuperDataFrameMonolitico:

    def __init__(self):
        self.exps = self.load_all_port_scanners()

        # init df
        self.rows = []
        for jobname, scanners in self.exps.items():
            if 'prod' in jobname:
                for sc in scanners:
                    d = {
                        'jobname': jobname,
                        'root_server_ip': sc.server_ip,
                        'url': sc.url,
                        'scanner_object': sc
                    }
                    self.rows.append(d)

        self.df = pd.DataFrame(self.rows)

    def load_all_port_scanners(self):
        exp_objects = {}
        for jobname in os.listdir('data'):
            if jobname != 'bokita':
                exp_objects[jobname] = self.load_port_scanners_from_jobname(jobname)
        return exp_objects

    def load_port_scanners_from_jobname(self, jobname):
        port_scanners = []
        for port_scanner_filename in os.listdir('data/' + jobname):
            port_scanner_filepath = 'data/' + jobname + '/' + port_scanner_filename
            port_scanners.append(ExpRunner.load_object(port_scanner_filepath, verbose=False))

        return port_scanners

    def create_reduce_col(self, function_name, function):
        self.df[function_name] = self.df['scanner_object'].apply(function)


def get_leaves(sn):

    leaves = []
    _get_leaves(sn, leaves, 1)  # root node has height = 1

    return leaves


def _get_leaves(sn, leaves, height):
    if sn.children:
        for _, child in sn.children.items():
            _get_leaves(child, leaves, height+1)
    else:
        leaves.append((sn, height))


def average_leaf_depth(sc):
    leaves = get_leaves(sc)
    depths = [depth for leaf, depth in leaves]
    return mean(depths)


def is_soa(sc):
    if sc.response is None:  # packages can get lost due to DNS using UDP
        return False

    if sc.response.ns is not None:
        return sc.response.ns.type == 6

    return False


def soa_qty(sc):
    leaves = [leaf for leaf, depth in get_leaves(sc)]
    return sum(map(is_soa, leaves))


def root_name_servers_ip_list():
    root_servers = pd.read_csv('dns_root_servers.csv')
    return list(root_servers['IPv4 address'].values)


def get_info(url):
    url = 'http://ip-api.com/json/' + url
    info = requests.get(url=url, params={}).json()
    try:
        continent_code = pc.country_alpha2_to_continent_code(info['countryCode'])
        info.update({'continentCode': continent_code})
    except KeyError:
        info['countryCode'] = None
        info['continentCode'] = None
    return info


def enabled_mail_server_count(sc):
    leaves = get_leaves(sc)
    listDifferentUrls=differentUrls(leaves)
    counter=0
    for url in listDifferentUrls:
        if(ping(url)):
            counter=counter+1
    return counter

def differentUrls(leaves):
    listDifferentUrls=[]
    for leave in leaves:
        if(leave[0].response is not None):
            for it in range(leave[0].response.ancount):
                if(leave[0].response.an[it].exchange not in listDifferentUrls):
                    listDifferentUrls.append(leave[0].response.an[it].exchange)
    return listDifferentUrls

def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    # Option for the number of packets as a function of
    param = '-n' if platform.system().lower()=='windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    return subprocess.call(command) == 0

def get_ipv4_list(scanner):
    result_ips=[]
    leaves=get_leaves(scanner)
    for leave in leaves:
        result_ips=result_ips+leave[0].path_previous_ips
    return [x for x in result_ips if ':' not in x ]


def get_answer_list(scanner):
    answers=[]
    leaves=get_leaves(scanner)
    for leave in leaves:
        if leave[0].response is not None:
            if leave[0].response.ancount>0:
                for x in range(leave[0].response.ancount):
                    answers.append(leave[0].response.an[x].exchange)
    return answers
    
def ipv4(ip):
    return type(ipaddress.ip_address(ip)) == ipaddress.IPv4Address
