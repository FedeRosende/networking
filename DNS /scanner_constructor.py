from scanner import FullScanner, QuickScanner


class ScannerConstructor:

    def construct(**kwargs):
        if kwargs['full_tree'] is True:
            return FullScanner(**kwargs)
        else:
            return QuickScanner(**kwargs)
