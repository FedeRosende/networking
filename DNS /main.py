#!/usr/bin/python
import sys
sys.path.append('/home/oci/.pyenv/versions/3.6.5/lib/python3.6/site-packages')
from scanner_constructor import *

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Send me an URL to execute program. Example: exactas.uba.ar")
        sys.exit(0)

    url = sys.argv[1]
    root_server = "199.9.14.201"

    scn_config = {'url': url, 
		'server_ip': root_server, 
		'verbose': True, 
		'full_tree': False}
    sn = ScannerConstructor.construct(**scn_config)
    sn.run()
    print("\n ----------- \n")
    if sn.final_answer:
        print("MX servers: {}".format(sn.final_answer))
    else:
        print("Answer was not found for {}".format(url))
