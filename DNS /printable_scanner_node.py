import pptree


class PrintableScannerNode:

    def __init__(self, ip, sn, head=None):
        self.ip = ip
        self.sn = sn
        self.childs = []
        if head:
            head.childs.append(self)

    def __str__(self):
        return self.ip
    
    def _create_printable_tree(self):
        if self.sn:
            l = self.sn.children.items()
            for ip, sn in l:
                new_node = PrintableScannerNode(ip, sn, self)
                new_node._create_printable_tree()
    
    def print_tree(self):
        self._create_printable_tree()
        return pptree.print_tree(self, "childs")
