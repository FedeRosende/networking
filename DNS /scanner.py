import sys
from scapy.all import *
import ipaddress


class Scanner:

    DNS_MAIL_EXCHANGE = scapy.layers.dns.DNSRRMX
    DNS_SOA = scapy.layers.dns.DNSRRSOA
    MX_TYPE = 15
    IPV4_TYPE = ipaddress.IPv4Address

    def __init__(self, path_previous_ips, **kwargs):
        if path_previous_ips is None:
            path_previous_ips = []

        self.server_ip = kwargs['server_ip']
        self.url = kwargs['url']
        self.verbose = kwargs['verbose']
        self.full_tree = kwargs['full_tree']

        self.response = None
        self.final_answer = []
        self.path_previous_ips = path_previous_ips  # avoid cycles
        self.closed = False
        self.leaves = []

    def run(self):
        if not self.closed:
            if self.server_ip in self.path_previous_ips:
                self.response = 'Cycle'
            else:
                self.get_response()
                self.path_previous_ips.append(self.server_ip)
                self.run_children_ips()
            self.closed = True
            return self.final_answer

        else:
            print("Scanner closed")

    def get_response(self):
        if self.verbose: print("Next ip to work with is: {}".format(self.server_ip))

        ip_type = type(ipaddress.ip_address(self.server_ip))

        if not ip_type == Scanner.IPV4_TYPE:
            self.response = None
        else:
            ip = IP(dst=self.server_ip)
            dns = DNS(rd=1, qd=DNSQR(qname=self.url, qtype="MX"))
            udp = UDP(sport=RandShort(), dport=53)
            self.response = sr1(ip / udp / dns, verbose=0, timeout=10)
            self.print_response()

    def has_an_authority_response(self):
        return (self.response is not None) and (self.response.ancount == 0) and (self.response.arcount > 0)

    def has_a_mail_exchange_answer(self):
        if (self.response is not None) and (self.response.ancount > 0):
            for i in range(self.response.ancount):
                if self.mail_exchange(self.response.an[i]):
                    return True
        return False

    def print_response(self):
        if self.response is not None:
            if self.response.haslayer(DNS) and self.response[DNS].qd.qtype == Scanner.MX_TYPE:

                self.print_verbose("AUTHORITY", self.response.arcount, self.response.ar)
                self.print_verbose("NAME_SERVERS", self.response.nscount, self.response.ns)
                self.print_verbose("ANSWER", self.response.ancount, self.response.an)

    def mail_exchange(self, dns_elem):
        return type(dns_elem) == Scanner.DNS_MAIL_EXCHANGE

    def soa(self, dns_elem):
        return type(dns_elem) == Scanner.DNS_SOA

    def get_attr(self, attr):
        if type(attr) == str:
            return attr
        return attr.decode('utf-8')

    def print_verbose(self, name, size, l):
        if self.verbose:
            print(name)
            for i in range(size):
                name = self.get_attr(l[i].rrname)
                data = None
                if self.mail_exchange(l[i]):
                    data = self.get_attr(l[i].exchange)
                else:
                    if self.soa(l[i]):
                        name = self.get_attr(l[i].rname)
                        data = "SOA"
                    else:
                        data = self.get_attr(l[i].rdata)
                print("{}, {}".format(name, data))

    def run_children_ips(self):
        raise Exception("implementation is specifical for every scanner subtype")


class FullScanner(Scanner):

    def __init__(self, path_previous_ips=None, **kwargs):
        super().__init__(path_previous_ips, **kwargs)
        self.children = {}

    def run_children_ips(self):
        if self.has_a_mail_exchange_answer():
            ans = [self.get_attr(self.response.an[i].exchange) for i in range(self.response.ancount)]
            self.final_answer = ans
        if self.has_an_authority_response():
            for i in range(self.response.arcount):
                child_ip = self.response.ar[i].rdata

                if child_ip not in self.path_previous_ips:
                    kwargs = {'server_ip': child_ip, 'url': self.url, 'verbose': self.verbose, 'full_tree': True}
                    child_scanner = FullScanner(
                        path_previous_ips=list(self.path_previous_ips),
                        **kwargs
                    )
                    self.children[child_ip] = child_scanner
                    child_scanner.run()
                    self.final_answer = self.final_answer + child_scanner.final_answer
        self.final_answer = list(set(self.final_answer))


class QuickScanner(Scanner):

    def __init__(self, path_previous_ips=None, **kwargs):
        super().__init__(path_previous_ips, **kwargs)

    def run_children_ips(self):
        if self.has_a_mail_exchange_answer():
            ans = [self.get_attr(self.response.an[i].exchange) for i in range(self.response.ancount)]
            self.final_answer = ans
        if self.has_an_authority_response():
            for i in range(self.response.arcount):
                child_ip = self.response.ar[i].rdata

                if child_ip not in self.path_previous_ips:
                    kwargs = {'server_ip': child_ip, 'url': self.url, 'verbose': self.verbose, 'full_tree': False}
                    child_scanner = QuickScanner(
                        path_previous_ips=list(self.path_previous_ips),
                        **kwargs
                    )
                    child_response = child_scanner.run()
                    if child_response is not None:
                        self.final_answer = child_response
                        break
