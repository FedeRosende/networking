#!/usr/bin/python

import sys

from scapy.all import *
from time import *
from collections import defaultdict
import numpy as np
from datetime import datetime

from college_ip import *


def filter_outliers(nums, threshold=3):
    # computes Z-score and keeps values below threshold
    nums = np.array(nums)
    avg = np.mean(nums)
    std = np.std(nums)
    if std == 0:
        return nums

    valid_values_mask = np.abs((nums - avg) / std) < threshold

    return nums[valid_values_mask]


def curr_time():
    now = datetime.now()
    return now.strftime("%d_%m_%YT%H_%M")


def is_continental_jump(src_ip, dest_ip):

    # takes ip classes as params

    return continent_codes_differ(src_ip.info['continentCode'], dest_ip.info['continentCode'])


def continent_codes_differ(src_code, dest_code):

    # takes strings as params

    if src_code is None or dest_code is None:
        return None

    return src_code != dest_code


class RouteTracer:

    def __init__(self, **kwargs):
        self.ip = kwargs['ip']
        self.retry_qty = kwargs['retry_qty']
        self.max_ttl = kwargs['max_ttl']
        self.verbose = True
        self.min_qty = 1/3

        # responses[ttl][ip] will contain a list of rtt's
        self.responses = defaultdict(
            lambda: defaultdict(list))  # use lambda notation because defaultdict(x) requires x to be a callable
        self.top_responses_ips = []
        self.filtered_top_responses_ips = []
        self.differences = []
        self.run_datetime = None

    def get_responses(self):
        # get rtt
        for i in range(self.retry_qty):
            if self.verbose: print('Running retry {}'.format(i))
            address_reached = False
            for ttl in range(1, self.max_ttl + 1):
                probe = IP(dst=self.ip.address, ttl=ttl) / ICMP(id=100)
                t_i = time()
                ans = sr1(probe, verbose=False, timeout=0.8)
                t_f = time()
                rtt = (t_f - t_i) * 1000
                if ans is not None:
                    if ans.src == self.ip.address:
                        if address_reached:
                            break
                        else:
                            address_reached = True
                    self.responses[ttl][ans.src].append(rtt)

    def get_most_responding_ips(self):
        # for each ttl, process rtt to get most responding ip's average response time
        # returns a list of tuples with the structure [(ttl_i, avg_rtt_i)]

        for ttl, ttl_responses in self.responses.items():
            # get most responding ip
            most_responding_ip = None
            longest_rtt_list = []
            for curr_ip, rtt_list in ttl_responses.items():
                if len(rtt_list) > len(longest_rtt_list):
                    longest_rtt_list = rtt_list
                    most_responding_ip = curr_ip

            if most_responding_ip:
                longest_rtt_list = filter_outliers(longest_rtt_list)
                if len(longest_rtt_list)/self.retry_qty > self.min_qty:
                    avg_response_time = np.mean(longest_rtt_list)
                    avg_response_time = round(avg_response_time, 5)
                    self.top_responses_ips.append((ttl, avg_response_time, RawIP(most_responding_ip)))

    def filter_non_increasing_rtts(self):
        # filter non-increasing rtt's
        self.filtered_top_responses_ips = list(self.top_responses_ips)
        i = 1
        while i < len(self.filtered_top_responses_ips):
            curr_ttl, curr_mean_rtt, _ = self.filtered_top_responses_ips[i - 1]
            next_ttl, next_mean_rtt, _ = self.filtered_top_responses_ips[i]

            if curr_mean_rtt >= next_mean_rtt:  # is next mean rtt non-increasing?
                del self.filtered_top_responses_ips[i]  # remove next elem
            else:
                # next mean rtt is valid
                i += 1

    def calculate_differences(self):
        # initialize differences list with first jump
        self.differences = []
        if self.filtered_top_responses_ips:
            next_ttl, next_mean_rtt, next_enriched_ip = self.filtered_top_responses_ips[0]

            self.differences.append(
                {
                    'src': 0,
                    'dst': next_ttl,
                    'rtt_diff': next_mean_rtt,
                    'is_continental_jump': continent_codes_differ('SA', next_enriched_ip.info['continentCode'])
                }
            )

        i = 0
        while i < len(self.filtered_top_responses_ips) - 1:
            curr_ttl, curr_mean_rtt, curr_enriched_ip = self.filtered_top_responses_ips[i]
            next_ttl, next_mean_rtt, next_enriched_ip = self.filtered_top_responses_ips[i + 1]

            self.differences.append(
                {
                    'src': curr_ttl,
                    'dst': next_ttl,
                    'rtt_diff': next_mean_rtt - curr_mean_rtt,
                    'is_continental_jump': is_continental_jump(curr_enriched_ip, next_enriched_ip)
                }
            )
            i += 1

    def run(self):
        if self.verbose: print("Running with {} retries and {} max_ttl with IP {} \n".format(
                                                                    self.retry_qty, self.max_ttl, self.ip.address))

        self.run_datetime = curr_time()

        self.get_responses()

        if self.verbose: print('\nget rtt')
        for ttl, response_ttl in self.responses.items():
            if len(response_ttl) > 0:
                for src in response_ttl.keys():
                    if self.verbose: print(ttl, src, self.responses[ttl][src])

        self.get_most_responding_ips()

        # rtt differences

        self.top_responses_ips.sort()  # this will sort by ttl_index (first element of tuple)

        if self.verbose: print('\nprocess rtt')
        if self.verbose: print(self.top_responses_ips)

        self.filter_non_increasing_rtts()

        if self.verbose: print('\nfilter processed rtt')
        if self.verbose: print(self.filtered_top_responses_ips)

        self.calculate_differences()

        if self.verbose: print('\ncalculate differences')
        if self.verbose: print(self.differences)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Give me an ip as argument. Example: 157.92.27.128")
        sys.exit()

    ip = RawIP(sys.argv[1])

    # default configs
    max_ttl = 25
    retry_qty = 5

    if len(sys.argv) > 2:
        max_ttl = int(sys.argv[2])

    if len(sys.argv) > 3:
        retry_qty = int(sys.argv[3])

    rt_config = {'ip': ip, 'retry_qty': retry_qty, 'max_ttl': max_ttl}

    rt = RouteTracer(**rt_config)
    rt.run()
