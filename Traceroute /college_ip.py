import socket
import requests
import abc
import pycountry_convert as pc
from memoized_property import memoized_property


class RawIP:
    GEO_DATA_URL = "http://ip-api.com/json/"

    def __init__(self, ip_address):
        self.address = ip_address
        self.url = None

    def __str__(self):
        return self.address
    
    def __repr__(self):
        return self.address

    @memoized_property
    def info(self):
        url = RawIP.GEO_DATA_URL + self.address
        info = requests.get(url=url, params={}).json()
        try:
            continent_code = pc.country_alpha2_to_continent_code(info['countryCode'])
            info.update({'continentCode': continent_code})
        except KeyError:
            info['countryCode'] = None
            info['continentCode'] = None
        return info

    @property
    @abc.abstractmethod
    def clean_url(self):
        pass


class CollegeIP(RawIP):
    
    def __init__(self, url):
        self.url = url
    
    @property
    def clean_url(self):
        lowercase = self.url.lower()
        no_http = lowercase.replace("http://", "").replace("https://", "").replace("/", "")
        return no_http
    
    @property
    def address(self):
        return socket.gethostbyname(self.clean_url)

