import os
import dill

from main import *


class ExpRunner:

    def __init__(self, rt_config, jobname, save_data=True, prefer_loading_saved_data=True):
        self.rt_config = rt_config
        self.jobname = jobname
        self.save_data = save_data
        self.prefer_loading_saved_data = prefer_loading_saved_data
        self.route_tracer = None

    @property
    def file_dump_path(self):
        filename = 'route_tracer__' + self.jobname
        for k, v in self.rt_config.items():
            filename += '__' + str(k) + '_' + str(v)

        filename += '.pkl'

        return 'data/' + self.jobname + '/' + filename

    def run_exp(self, verbose=True):
        if self.prefer_loading_saved_data and os.path.exists(self.file_dump_path):
            # run exp only if it has not been run before
            with open(self.file_dump_path, 'rb') as f:
                print("File was found. Loading saved data")
                self.route_tracer = dill.load(f)
        else:
            self.construct_dir()
            self.route_tracer = RouteTracer(**self.rt_config)
            self.route_tracer.verbose = verbose
            self.route_tracer.run()

        if self.save_data:
            with open(self.file_dump_path, 'wb') as f:
                dill.dump(self.route_tracer, f)

        return self.route_tracer
    
    def construct_dir(self):
        if self.save_data:
            d = 'data/' + self.jobname
            if not os.path.isdir(d):
                os.mkdir(d)


